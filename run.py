#!/usr/bin/env python3

import argparse
import os
import sys

import mutagen.flac
import mutagen.id3
import mutagen.mp3


def process_musics(music_dir: str, hierarchy: tuple) -> None:

    print(f'Entering "{"/".join(hierarchy)}" ...')

    base_dir = os.path.join(music_dir, *hierarchy)

    for entry in os.listdir(base_dir):

        path = os.path.join(base_dir, entry)

        if os.path.isdir(path):
            process_musics(music_dir, (*hierarchy, entry))

        elif os.path.isfile(path):

            filename_tags = os.path.splitext(entry)[0].split(' - ')
            assert len(filename_tags) in [2, 4]

            file = mutagen.File(path)
            
            if isinstance(file, mutagen.flac.FLAC):

                tags = mutagen.flac.VCFLACDict()
                if len(filename_tags) == 2:
                    tags['artist'] = [filename_tags[0]]
                    tags['title'] = [filename_tags[1]]
                else:
                    tags['artist'] = [filename_tags[0]]
                    tags['album'] = [filename_tags[1]]
                    tags['tracknumber'] = [filename_tags[2]]
                    tags['title'] = [filename_tags[3]]

                if tags != file.tags:
                    file.clear_pictures()

            elif isinstance(file, mutagen.mp3.MP3):

                tags = mutagen.id3.ID3()
                if len(filename_tags) == 2:
                    tags['TPE1'] = mutagen.id3.TPE1(text=[filename_tags[0]])
                    tags['TIT2'] = mutagen.id3.TIT2(text=[filename_tags[1]])
                else:
                    tags['TPE1'] = mutagen.id3.TPE1(text=[filename_tags[0]])
                    tags['TALB'] = mutagen.id3.TALB(text=[filename_tags[1]])
                    tags['TRCK'] = mutagen.id3.TRCK(text=[filename_tags[2]])
                    tags['TIT2'] = mutagen.id3.TIT2(text=[filename_tags[3]])

            else:
                print(f'Unrecognized file will be ignored: "{entry}"')
                continue

            if tags != file.tags:

                keys_to_delete = list()
                for key in file.keys():
                    if key in tags.keys():
                        file[key] = tags[key]
                    else:
                        keys_to_delete.append(key)

                for key in keys_to_delete:
                    del file[key]

                file.save(path)

def main() -> int:

    parser = argparse.ArgumentParser(prog='Music Manager')
    parser.add_argument(
        'music_dir',
        action='store',
        type=str,
        help='',
        metavar='<music-dir>')

    args = parser.parse_args()

    process_musics(args.music_dir, tuple())

    return 0


if __name__ == "__main__":
    sys.exit(main())
